# Measurements for out of sequence in Pub/Sub

I've made several experiments to measure out of sequence rate in Pub/Sub. The idea is to send 1000 messages to Pub/Sub topic with a certain delay (configurable) and measure:
- how many messages arrived out of sequence
- what is the average distance between consumed message timestamp and a message timestamp if it was consumed in order. 

For example, there are 4 messages produced with sequence numbers 1,2,3,4 and there are 4 messages consumed in order of 2,3,1,4, message with sequence number 2 consumed at ts2, message with sequence number 3 consumed at ts3 and so on. That means there are 3 messages received out of order. And avg distance between messages arrived out of sequence is calculated as avg( abs(ts2-ts1) + abs(ts3-ts2) + abs(ts1-ts3) ) 

Here is the results:

Experiment 1:
- average distance between produced messages:             25.8 ms
- total out of order messages:                            1000 / 1000
- average distance between consume and consume in order:  238 ms


Experiment 2:
- average distance between produced messages:             78 ms
- total out of order messages:                            995 / 1000
- average distance between consume and consume in order:  79 ms


Experiment 3:
- average distance between produced messages:             101 ms
- total out of order messages:                            0 / 1000
- average distance between consume and consume in order:  0 ms

Experiment 4:
- average distance between produced messages:             145 ms
- total out of order messages:                            4 / 1000
- average distance between consume and consume in order:  228 ms

Experiment 5:
- average distance between produced messages:             531 ms
- total out of order messages:                            0 / 1000
- average distance between consume and consume in order:  0 ms


## Conclusion
Based on the results above we can see that the threshold is 100 ms. There is a high probability of getting messages out of sequence if they're produced with less than 100 ms apart. Otherwise probability of getting messages out of sequence is low.
However there are spikes with getting messages out of order that can reach the distance of 1 second or more, I saw a 968 ms spike and I believe it's not the maximum possible.

## How to reproduce

- build project

```./gradlew clean build```

- run reader:

```java -jar ReadFromPubsub/build/libs/ReadFromPubsub-1.0-SNAPSHOT-all.jar five9dataservices console-string_events```

where `five9dataservices` is a gcp project name and `console-string_events` is a Pub/Sub subscription to listen to

- run writer:

```java -jar WriteToPubsub/build/libs/WriteToPubsub-1.0-SNAPSHOT-all.jar five9dataservices string_events 1000 75```

where:

    - "five9dataservices" is a gcp project name
    - "string_events" is a Pub/Sub topic to publish messages to
    - 1000 is number of messages to publish
    - 75 is a minimum delay between messages

- copy paste results (from the reader console) to a csv file

- create a table in Google BigQuery and upload the csv file with the folowing table definition:
```
[
    {
        "name": "seq_producer",
        "type": "INTEGER",
        "mode": "REQUIRED"
    },
    {
        "name": "ts_producer",
        "type": "NUMERIC",
        "mode": "REQUIRED"
    },
    {
        "name": "seq_consumer",
        "type": "INTEGER",
        "mode": "REQUIRED"
    },
    {
        "name": "ts_consumer",
        "type": "NUMERIC",
        "mode": "REQUIRED"
    }
]
```

- run the following queries to get the results (assuming you created a table ooo_pubsub in dataset out_of_order):
    - get average distance between produced messages:
    ```
    select avg(abs(ts1-ts2))
    from
    (select seq_producer seq1, ts_producer ts1 from out_of_order.ooo_pubsub) t1 inner join
    (select seq_producer seq2, ts_producer ts2 from out_of_order.ooo_pubsub) t2 on t1.seq1 = t2.seq2+1
    ```

    - get total number of messages that came out of order:
    ```
    select count(seq_producer)
    from out_of_order.ooo_pubsub
    where seq_producer != seq_consumer
    ```

    - get average distance between message consume time and message in order consume time (desired):
    ```
    select avg(abs(t1.ts1 - t2.ts2))
    from 
      (select seq_producer seq1, ts_consumer ts1 from out_of_order.ooo_pubsub) t1 inner join
      (select seq_consumer seq2, ts_consumer ts2 from out_of_order.ooo_pubsub) t2 on t1.seq1 = t2.seq2
    ```

    - get max distance (peak) between message consume time and message in order consume time (desired):
    ```
    select max(abs(t1.ts1 - t2.ts2))
    from 
      (select seq_producer seq1, ts_consumer ts1 from out_of_order.ooo_pubsub) t1 inner join
      (select seq_consumer seq2, ts_consumer ts2 from out_of_order.ooo_pubsub) t2 on t1.seq1 = t2.seq2
    ```
